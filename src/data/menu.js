export default [
    {
        title: 'Home',
        url: '/'
    },
    {
        title: 'Game',
        url: '/game'
    },
    {
        title: 'Counter',
        url: '/counter'
    }
];