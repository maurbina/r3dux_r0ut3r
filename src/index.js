//Dependiencies
import React from "react";
import RenderDOM from "react-dom";
//npm install react-router-dom
import { BrowserRouter as Router } from "react-router-dom";

//Routes
import AppRoutes from "./routes";

//Assets
import "./index.css";

RenderDOM.render(
  <Router>
    <AppRoutes />
  </Router>,
  document.getElementById("root")
);
