//Dependencies
import React, { Component } from "react";
import PropTypes from 'prop-types';
//npm install redux react-redux
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from '../store/reducers';

//Components
import Header from "./Global/Header";
import Content from "./Global/Content";
import Footer from "./Global/Footer";

//Data
import items from '../data/menu';

// Assets
import './App.css';

const store = createStore(reducer);

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  }
  render() {
    const { children } = this.props;
    return (
      <div className="App">
        <Header title={"Redux and  Router"} items={items} />
        <Provider store={store}>
          <Content body={children} /> 
        </Provider>
        <Footer copyrigth="&copy; Copyrigth" />
      </div>
    );
  }
}

export default App;
