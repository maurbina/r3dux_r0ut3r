//Dependencies
import React, { Component } from "react";
import PropTypes from 'prop-types';

//Assets
import "./css/Footer.css";

class Footer extends Component {
  static propTypes = {
    copyrigth: PropTypes.string
  };

  render() {
     const { copyrigth = "&copy; Copyrigth"} = this.props;
    return (
      <div className="Footer">
        <p dangerouslySetInnerHTML= {{ __html : copyrigth }} />
      </div>
    );
  }
}

export default Footer;
