//Dependencies
import React from "react";
import { connect } from "react-redux";

//Assets
import './index.css';

const Counter = props => {
  return (
    <div className="counter">
      <h1>Counter with Redux </h1>
      <h3> { props.count } </h3>
      <div className="buttons">
          <button onClick={props.onClickIncrement}>Increment</button>
          <button onClick={props.onClickReset}>Reset</button>
          <button onClick={props.count > 0 ? props.onClickDecrement : false }>Decrement</button>
      </div>
    </div>
  );
};

function mapStateProps(state) {
    return {
        count: state.count
    }
}

function mapDispatchToProps(dispatch) {
    return {
      onClickIncrement: () => {
        const action = { type: "INCREMENT" };
        dispatch(action);
      },
      onClickDecrement: () => {
        const action = { type: "DECREMENT" };
        dispatch(action);
      },
      onClickReset: () => {
        const action = { type: "RESET"};
        dispatch(action);
      }
    };
  }

export default connect(mapStateProps,mapDispatchToProps)(Counter);
