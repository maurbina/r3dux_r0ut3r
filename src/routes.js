//Dependencies
import React from "react";
import { Switch, Route } from "react-router-dom";

//Componentes
import App from "./components/App";
import Game from "./components/Game";
import Counter from "./components/Counter";
import Home from "./components/Home";
import Page404 from "./components/Page404";

const AppRoutes = () => (
  <App>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/game" component={Game} />
      <Route exact path="/counter" component={Counter} />
      <Route exac component={Page404} />
    </Switch>
  </App>
);

export default AppRoutes;
